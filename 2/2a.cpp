#include <iostream>
#include <string>
#include <vector>

using namespace std;


int main()
{
	setlocale(LC_ALL, "RUS");

	int n;
	cin >> n; // вводим n

	std::vector<string> output;

	if (n >= 1 && n <= 1000) {
		for (int i = 0; i <= n; i++) {
			string command; // строка для команды
			getline(cin, command); // считываем строку которую ввел пользователь
			if (command.substr(0, 3) == "Run") {  // если это комманда Run
				string s = command.substr(4, 100); // то берем название приложения 
				output.push_back(s); // и добавляем в вывод и делаем его активным
			}
			else if (command.substr(0, 3) == "Alt") { // если это комманда Alt
				string s = command.substr(3); // получаем строку с +Tab
				int count = s.size() / 4; // считаем сколько раз нажата Tab
				int active = (count % n); // получаем индекс активного приложения по условию
				output.push_back(output[output.size() - active - 1]); // добавляем его в вывод и делаем активным
			}
		}

		for (int i = 0; i < output.size(); i++) {
			cout << output[i] << endl; // выводим результат
		}
	}

    return 0;
}

