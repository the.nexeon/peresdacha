#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "RUS");

	int n;
	cin >> n;

	if (n > 0 && n <= 100) {
		int** ar = new int*[n]; // создаем массив
		for (int i = 0; i < n; ++i) { 
			ar[i] = new int[n]; // инициализируем массив
		}

		// вводим элементы массива
		int element = 0; 

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				cin >> element;
				ar[i][j] = element;
			}
		}


		bool sym = true; // симметрична ли матрица

		for (int i = 0; i < n; i++) { // проходимся по каждому элементу
			for (int j = i + 1; j < n; j++) { // спуск по диагонали 
				if (ar[i][j] != ar[j][i]) sym = false; // если элементы относительно диагонали отличаются, то массив не симметричен
			}
		}

		if (sym == true) {
			cout << "yes" << endl;
		}
		else {
			cout << "no" << endl;
		}
	}

    return 0;
}

