// ConsoleApplication2.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <openssl/conf.h> // функции, структуры и константы настройки OpenSSL
#include <openssl/conf.h>
#include <openssl/evp.h> // сами криптогрфические функции https://wiki.openssl.org/index.php/EVP
#include <openssl/err.h> // коды внутренних ошибок OpenSSL и их расшифровка
#include <openssl/aes.h>
#include <fstream>
#include <string>

using namespace std;

void enc() {
	unsigned char *crypted = new unsigned char[128]; // Шифр
	unsigned char *iv = (unsigned char *)"0123456789012345"; // рандомайзер
	unsigned char *password = (unsigned char *)"0123456789012345"; // пароль
	unsigned char *plaintext = new unsigned char[1000]; // дешифрованная строка
	plaintext = (unsigned char *)"Hello World!";
	int len; // Длина полученного шифра
	int ciphertext_len; // Длина шифра после обработки

	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new(); // Создание структуры с настройками метода

	EVP_EncryptInit_ex(ctx, // ссылка на объект/структуру, куда заносятся параметры
		EVP_aes_256_cbc(), // ссылка на шифрующее ядро AES 256 (функцию с алгоритмом)
		NULL,
		password, // ключ/пароль/секрет
		iv // рандомайзер (случайный начальный вектор)
	);

	EVP_EncryptUpdate(
		ctx, // объект с настройками
		crypted, // входной параметр: ссылка, куда помещать зашифрованные данные
		&len, // выходной параметр: длина полученного шифра
		(unsigned char*)plaintext, // входной параметр: что шифровать
		strlen((const char*)plaintext)
	); // входной параметр : длина входных данных

	ciphertext_len = len;

	// 5. Финализация процесса шифрования
	// необходима, если последний блок заполнен данными не полностью
	EVP_EncryptFinal_ex(ctx, crypted + len, &len);
	ciphertext_len += len;

	// 6. Удаление структуры
	EVP_CIPHER_CTX_free(ctx); // Освобождение памяти

	ofstream out;          // поток для записи
	out.open("enc.dat", ofstream::binary); // окрываем файл для записи
	if (out.is_open())
	{
		out << strlen((const char*)plaintext) << endl << crypted;
	}

	out.close();

}

void dec() {
	std::string line;
	unsigned char *crypted = new unsigned char[128]; // Шифр
	int crypted_len = 0;

	std::ifstream in("enc.dat", ofstream::binary); // окрываем файл для чтения
	if (in.is_open())
	{
		getline(in, line);
		crypted_len = atoi(line.c_str());
		getline(in, line);
		crypted = (unsigned char *)line.c_str();
	}
	in.close();     // закрываем файл

	unsigned char *iv = (unsigned char *)"0123456789012345"; // рандомайзер
	unsigned char *password = (unsigned char *)"0123456789012345"; // пароль
	unsigned char *plaintext = new unsigned char[1000]; // дешифрованная строка
	int len; // Длина строки
	int plaintext_len; // Длина строки после обработки

	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new(); // Создание структуры с настройками метода

	EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, password, iv); // Инициализация методом AES, ключом и вектором

	EVP_DecryptUpdate(ctx, (unsigned char*)plaintext, &len, crypted, 1000); // Шифрование

	plaintext_len = len;
	EVP_DecryptFinal_ex(ctx, (unsigned char*)plaintext + len, &len); // Финальная обработка

	plaintext_len += len;
	EVP_CIPHER_CTX_free(ctx); // Освобождение памяти

	plaintext[crypted_len] = '\0'; // Обозначаем конец строки
	cout << plaintext;

}

int main()
{
	enc();
	dec();
	system("pause");
	return 0;
}

